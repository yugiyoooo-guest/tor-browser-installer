# MIT License
#
# Copyright (c) 2019 LoveIsGrief
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import subprocess
import tarfile

from installer.base import BaseInstaller

EXECUTABLE_NAME = "start-tor-browser.desktop"


class LinuxInstaller(BaseInstaller):
    def launch(self, fork=False):
        if not os.path.isdir(self.target_dir):
            raise FileNotFoundError("Please install tor-browser at: {}"
                                    .format(self.target_dir))

        # Find executable
        exec_file = None
        for root, dirs, files in os.walk(self.target_dir):
            exec_file = next(iter(
                os.path.join(root, file)
                for file in files
                if file == EXECUTABLE_NAME
            ), None)
            if exec_file:
                break

        if not exec_file:
            raise FileNotFoundError("Couldn't find {executable} anywhere in {root}".format(
                executable=EXECUTABLE_NAME,
                root=self.target_dir
            ))
        exec_dir = os.path.dirname(exec_file)
        if fork:
            # Launch in a forked child-process
            # So that this process can end
            child_pid = os.fork()
            if child_pid:
                self._l.info("PID %s for process %s", child_pid, exec_file)
            else:
                subprocess.run(exec_file,
                               cwd=exec_dir
                               )
        else:
            with subprocess.Popen(exec_file, cwd=exec_dir) as proc:
                self._l.info("PID %s for active child process %s", proc.pid, exec_file)

    def install(self):
        super(LinuxInstaller, self).install()
        self._l.info("Installing to %s", self.target_dir)
        with tarfile.open(self.download_path) as f:
            f.extractall(self.target_dir)

    @property
    def filename(self):
        return "tor-browser-linux{bits}-{version}_{language}.tar.xz".format(
            bits=self.bits,
            version=self.version,
            language=self.language
        )
