# MIT License
#
# Copyright (c) 2019 LoveIsGrief
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import platform

from installer.base import BaseInstaller


def get_installer(version, language=None, target_dir=None) -> BaseInstaller:
    os_type = platform.system().lower()
    bits = platform.architecture()[0].lower().replace('bit', '')
    if os_type == "linux":
        from installer.linux import LinuxInstaller
        installer_class = LinuxInstaller
    elif os_type == "osx":
        from installer.osx import OSXInstaller
        installer_class = OSXInstaller
    elif os_type == "windows":
        from installer.windows import WindowsInstaller
        installer_class = WindowsInstaller
    else:
        raise Exception("Unknown platform {} {}".format(os_type, bits))
    return installer_class(
        bits, version,
        language=language,
        target_dir=target_dir
    )
