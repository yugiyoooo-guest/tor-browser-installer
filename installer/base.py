# MIT License
#
# Copyright (c) 2019 LoveIsGrief
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


import logging
import os
from abc import abstractmethod, ABC
from tempfile import TemporaryDirectory

import requests

BASE_URL = "https://dist.torproject.org/torbrowser/"


class BaseInstaller(ABC):
    """

    :type _download_dir TemporaryDirectory
    """

    def __init__(self, bits, version, language=None, target_dir=None):
        self.bits = bits
        self.version = version
        self.language = language or "en-US"
        # TODO force init in subclasses
        self.target_dir = target_dir or os.path.expanduser(
            "~/programs/torbrowser/{}".format(version)
        )
        # noinspection PyTypeChecker
        self._download_dir = None
        self.download_path = None
        self._l = logging.getLogger(self.__class__.__name__)

    def __enter__(self):
        if not self._download_dir:
            self._download_dir = TemporaryDirectory(
                prefix='tor-installer',
                suffix=self.__class__.__name__
            )
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self._download_dir.cleanup()
        # noinspection PyTypeChecker
        self._download_dir = None

    @property
    def filename(self):
        raise NotImplementedError()

    @property
    def download_url(self):
        return "{base}/{version}/{filename}".format(
            base=BASE_URL,
            version=self.version,
            filename=self.filename
        )

    def download(self):
        local_filename = os.path.join(self._download_dir.name, self.filename)
        self._l.info("Downloading: %s", self.download_url)
        r = requests.get(self.download_url, stream=True)
        r.raise_for_status()
        with open(local_filename, 'wb') as f:
            for chunk in r.iter_content(chunk_size=1024):
                if chunk:  # filter out keep-alive new chunks
                    f.write(chunk)
            f.flush()
        self.download_path = local_filename

    @abstractmethod
    def install(self):
        if not self.download_path:
            raise FileNotFoundError("tor-browser not downloaded")

    def execute(self):
        self.download()
        self.install()

    @abstractmethod
    def launch(self, fork=False):
        """
        Run the installed application
        """
        raise NotImplementedError()
