# MIT License
#
# Copyright (c) 2019 LoveIsGrief
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import logging
import re

from requests_html import HTMLSession, HTMLResponse

from installer import get_installer
from installer.base import BASE_URL

VERSION_REGEX = re.compile(r"\d+\.[\w.]+")


def get_versions():
    session = HTMLSession()
    # noinspection PyTypeChecker

    r = session.get(BASE_URL)  # type: HTMLResponse
    # Get the list of available versions
    anchors = r.html.find('img[alt="[DIR]"]+a')
    links = []
    for anchor in anchors:
        link = anchor.attrs.get('href')
        if not link:
            continue
        match = VERSION_REGEX.match(link)
        if not match:
            continue
        links.append(match.group(0))
    sorted(links)
    return links


def main(arguments):
    logging.basicConfig(level=logging.INFO)
    versions = get_versions()
    logger = logging.getLogger("tor-browser-installer")
    logger.info("Found versions:\n\t%s", '\n\t'.join(versions))
    if arguments.list_versions:
        return

    # Select the most recent version
    version = arguments.install_version or versions[-1]
    logger.info("Selected version: %s", version)
    with get_installer(version,
                       language=arguments.language,
                       target_dir=arguments.target) as installer:
        installer.execute()

        if arguments.auto_launch:
            installer.launch()


if __name__ == '__main__':
    from argparse import ArgumentParser

    parser = ArgumentParser('tor-browser-installer', description="""
    Detects your platform an installs tor-browser in a chose or default location
    """)

    parser.add_argument("-t", "--target",
                        help="Where should tor-browser be installed to")
    # TODO download all possible supported languages
    parser.add_argument("-n", "--language",
                        default='en-US',
                        help="The language code e.g en-US")
    parser.add_argument("-i", "--install-version",
                        help="Which version to install. The chosen version might not be available!")
    parser.add_argument("-l", "--list-versions",
                        action='store_true',
                        help="List available versions to install")

    parser.add_argument("--auto-launch",
                        action='store_true',
                        help="Launch TOR-Browser after the download")

    main(parser.parse_args())
